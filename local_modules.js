const fs = require('fs');
const request = require('request');
const cheerio = require('cheerio');

module.exports.getHtml = (filename, url) => {
    request({uri: url}, function(error, response, body) {
        if(error){
            console.log(`${error}`);
            return;
        }
        if(response.statusCode < 200 || response.statusCode > 299){
            console.log(`Someting wen\'t wrong.\nReponse status code: ${response.statusCode}`);
            return;
        }
        parseHtml(filename, body);
    });
}

const parseHtml = (filename, body) => {
    const result = [];
    const $ = cheerio.load(body);
    const list = $('.pj-ui-product-listing-row').children('li').each((i, e) => {
        const url = $(e).children('div').last().children('a').last().attr('href');
        const name = $(e).children('div').last().children('a').first().children('div').last().text();
        const price = $(e).children('div').last().children('a').first().children('span').last().text();
        if(name==''){
            return;
        }
        result.push({
            url,
            name,
            price
        });
    });
    writeToFile(filename, result);
}

const writeToFile = (fileName, data) => {
    fs.writeFile(`results/${fileName}.json`, JSON.stringify(data, null, 2), err => {
        if(err){
            console.log(`Error: ${err}`);
            return;
        }
        console.log('Data written to file!');
    });
}