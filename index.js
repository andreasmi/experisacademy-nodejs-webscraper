const readline = require('readline');
const fs = require('fs');
const { getHtml } = require('./local_modules.js');
const { listenerCount } = require('process');

const express = require('express');
const { parseHTML } = require('cheerio');
const app = express();
const { PORT = 3000 } = process.env;

app.use(express.json());
app.use('/js', express.static(`${__dirname}/static`));
app.use(express.urlencoded({extended: true}));

const BASE_URL = 'https://www.prisjakt.no/search?search=';
let query = '';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


// Base Endpoint
app.get('/', (req, res) => {
    return res.status(200).sendFile(`${__dirname}/index.html`);
});

//Post a new search
app.post('/v1/search', (req, res) => {
    const userInput = req.body.userInput;
    getHtml(userInput, `${BASE_URL}${userInput}`);
    
    return res.status(201).redirect('/');
});

// Get the already created json files
app.get('/v1/results', (req, res) => {
    const result = [];
    fs.readdir('./results/', (err, files) => {
        if(err){
            console.log(err);
            return res.status(500).send('Something went wrong');
        }
        files.forEach(file => {
            result.push(file);
        });
        return res.status(200).send({
            result
        });
    });
    
});

//Get one file
app.get('/v1/results/:file', (req, res) => {
    const {file} = req.params;
    return res.status(200).sendFile(`${__dirname}/results/${file}`);
    
});


app.listen( PORT, () => {
    console.log(`Server started on port ${PORT}`);
    rl.question('What do you want to search for on Prisjakt?: ', (answer) => {
        if(answer == ''){
            console.log('\nThe field can\'t be empty');
            return;
        }
    
        getHtml(answer, `${BASE_URL}${answer}`);
    
    });
})

