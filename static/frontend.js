const elForm = document.getElementById('userForm');
const elBtnSend = document.getElementById('btnSearch');
const elList = document.getElementById('fileList');

elBtnSend.addEventListener('click', e => {
    elForm.submit();
})

async function getFiles(){
    try {
        await fetch('http://localhost:3000/v1/results')
                .then(res => res.json())
                .then(data => {
                    showFiles(data);
                });
    } catch (e) {
        console.log("Sometihng went wrong")
        console.log(e.message);
    }   
}

getFiles();

const showFiles = (data) => {
    [...data['result']].forEach(e => {
        createFileElement(e);
    })
}

const createFileElement = (name)=>{
    const elLi = document.createElement('li');
    elLi.className = 'list-group-item';

    const elA = document.createElement('a');
    elA.href = `/v1/results/${name}`;
    elA.innerText = name;

    elLi.appendChild(elA);
    elList.appendChild(elLi);
}