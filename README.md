# Experis Academy - NodeJs Webscraper

This is a webscraper application for NodeJs for the Noroff periode of Experis Academy

## Usage

Clone/fork repository. Run `npm install` to get the dependencies. To satrt the script, do `npm start`.
Either follow the instruction in the terminal or go to http://localhost:3000 for a frontend view of the solution.

### Known bugs

If you use the frontend application to do a new search, you need to refresh the page to get the .json file to be displayed. This is caused by the frontend being loaded before the backend is finished executing.

If you use `npm start` then the NodeJs script will restart after doing a new search. This is because of how nodemon is acting. To make it not restart, run `node index.js` instead.